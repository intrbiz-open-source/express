package com.intrbiz.express.dynamic;

import com.intrbiz.converter.Converter;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.validator.Validator;

/**
 * An expression helper interface which allows for model objects to pretend to be maps from  the view of the expression language.
 * For example:
 * <code><pre>
 * public class Test implements DynamicEntity {
 *     public Object get(String name, ExpressContext context, Object source) throws ExpressException {
 *         if ("abc".equals(name)) return 123;
 *         if ("def".equals(name)) return 456;
 *         return null;
 *     }
 * }
 * </pre></code>
 * Would enable expressions:
 * <code><pre>
 * #{test.abc}  => 123
 * #{test.def}  => 456
 * </pre></code>
 * 
 */
public interface DynamicEntity
{
	Object get(String name, ExpressContext context, Object source) throws ExpressException;
	
	default void set(String name, Object value, ExpressContext context, Object source) throws ExpressException {
	}
	
	default Converter<?> getConverter(String name, ExpressContext context, Object source) throws ExpressException {
	    return null;
	}
	
	default Validator<?> getValidator(String name, ExpressContext context, Object source) throws ExpressException {
	    return null;
	}
}
