package com.intrbiz.express.dynamic;

import com.intrbiz.express.ExpressContext;

/**
 * A proxy for an entity from the view of the expression language
 */
public interface EntityProxy
{
	Object getEntity(ExpressContext ctx, Object source);
}
